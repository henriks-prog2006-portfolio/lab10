use axum::{
    extract::Path,
    routing::{get, post},
    Json, Router,
};
use axum::http::StatusCode;
use serde::{Deserialize, Serialize};
use std::net::SocketAddr;
#[derive(Deserialize, Serialize)]
struct Greeting {
    greet: String,
    name: String,
    code: String,
}

#[derive(Deserialize, Serialize)]
struct WhateverText {
    input: String,
    name: String,
}

// Return a tuple of `String` and `StatusCode`, which implements `IntoResponse`
async fn hello() -> String {
    format!("Hello, World! {}", StatusCode::OK.to_string())
}

async fn error() -> String {
    format!("{}", StatusCode::NOT_FOUND.to_string())
}
async fn greet_user(Path(name): Path<String>) -> Json<Greeting> {
    let code = format!("{}", StatusCode::OK.to_string());
    Json(Greeting {
        greet: "Hello".to_string(),
        name,
        code,
    })
}

async fn greet_me(Json(item): Json<WhateverText>) -> Json<Greeting> {
    let code = format!("{}", StatusCode::OK.to_string());
    Json(Greeting {
        greet: item.input,
        name: item.name,
        code,
    })
}

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/hello", get(hello))
        .route("/", get(error))
        .route("/greet/:name", get(greet_user))
        .route("/greetme", post(greet_me));

    let addr = SocketAddr::from(([0, 0, 0, 0], 8000));
    println!("Listening on {}", addr);

    let listener = tokio::net::TcpListener::bind(&addr).await.unwrap();

    // Correct usage of `axum::serve`
    axum::serve(listener, app).await.unwrap();
}
