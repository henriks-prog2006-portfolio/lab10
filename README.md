# Lab10 - Hello JSON (Rust)


## Learning objectives for Lab10, as outlined by lecturer

> -  Learn how to build a simple webserver serving some simple API points, and handling JSON input/output.



----------------
### Prelim. info

*Crates used for this program:*

- axum = "0.7.5"
-  tokio = { version = "1.15.0", features = ["full"] }
-  serde = { version = "1.0", features = ["derive"] }
-  serde_json = "1.0"
-  http = "1.1.0"


### Task 1: GET "/hello"
```
GET "/hello" returns string: "Hello, World!" and 200 OK
```
#### Input:
```
localhost:8000/hello (in postman)
```
#### Output:
```
Hello, World! 200 OK
```
--------------
### Task 2: GET "/"
```
GET "/" returns 404 page not found
```
#### Input:
```
localhost:8000/
```
#### Output:
```
404 Not Found
```
--------------
### Task 3: GET "/greet/name"
```
GET "/greet/Mariusz" returns JSON { "greet": "Hello", "name": "Mariusz"} and 200 OK
```
#### Input:
```
localhost:8000/greet/Henrik
```
#### Output:
```
{
    "greet": "Hello",
    "name": "Henrik",
    "code": "200 OK"
}
```
--------------
### Task 4: Post "/greetme"
```
Makes sure endpoints that do not exist, returns the statusCode 404 page not found
```
#### Input:
```
http://localhost:8000/greetme
{ "input": "whatever text", "name": "Mariusz"}
```
#### Output:
```
{
    "greet": "whatever text",
    "name": "Mariusz",
    "code": "200 OK"
}
```
